import React from 'react';
import RegisterWrapper from './components/Register/RegWrapper/RegisterWrapper';
import LogInWrapper from './components/LogIn/LogWrapper/LogInWrapper';
import { BrowserRouter, Routes, Route} from "react-router-dom";
import UserSearchWrapper from './components/UserDisplay/UserSearch/UserSearchWrapper/UserSearchWrapper';
import AddChatWrapper from './components/AddChat/AddChatWrapper/AddChatWrapper';
import ChatPageDisplay from './components/ChatPage/ChatPageDisplay/ChatPageDisplay';
import SingleChatDisplay from './components/ChatPage/SingleChat/SingleChatDisplay/SingleChatDisplay';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route exact path="/" element={<RegisterWrapper />} />
        <Route path="login" element={<LogInWrapper />} />
        <Route path="home" element={<UserSearchWrapper />} />
        <Route exact path="chat/add" element={<AddChatWrapper/>} />
        <Route exact path="chats" element={<ChatPageDisplay/>}/>
        <Route exact path="card" element={<SingleChatDisplay/>}/>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
