import React from 'react';
import Alert from '@mui/material/Alert';

const AlertComponent = ({ error_msg }) =>{
    return(
        <Alert severity="error">{error_msg}</Alert>
    );
}

export default AlertComponent;