import axios from 'axios';
import URL from './Config'

const registerApi = async (user_to_register) =>{
    return await axios.post(URL+ '/register', user_to_register)
}

const loginApi = async (user_to_login) =>{
    return await axios.post(URL+'/login', user_to_login)
}

const getUserApi = async (partial_name) => {
    return await axios.get(URL + '/users' , partial_name)
}

const createChatApi = async (chat_to_add) => {
    return await axios.post(URL + '/chats/add',chat_to_add)
}

const getChatList = async (user_chats) => { 
    return await axios.get(URL + '/chats/get' , user_chats)
}

const getdMesagesApi = async (chatId) => { 
    return await axios.get(URL+ '/messages/get' , chatId)
}

export { registerApi , loginApi  , getUserApi , createChatApi  , getChatList , getdMesagesApi }