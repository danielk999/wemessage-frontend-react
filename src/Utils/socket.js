import Stomp from "stompjs";
import SockJS from "sockjs-client";
import URL from './Config'

const subscribe_channels = (socket , chats , handle_new_msg ) =>{
    for(const c of chats){
        socket.subscribe('/topic/room/'+c.id , handle_new_msg);
    }
};

const init_socket = (chats , handle_new_msg )=>{
    const SockJS_client = new SockJS(`${URL}/ws`);
    const socket = Stomp.over(SockJS_client);
    socket.connect({}, () => {
        console.log('------connected--------')
        subscribe_channels(socket , chats , handle_new_msg )
    } , ()=>console.log('error'));
    return socket;
}

export default init_socket;