import React from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import Avatar from '@mui/material/Avatar';
import Paper from '@mui/material/Paper';
import Button from '@mui/material/Button';

const UserCardDisplay = ({firstName , lastName , userName , email , show_create_chat_button , change_visability , create_chat }) => {
    
    
    //creates a color for each user using hashing
    const string_to_color = (name) => {
        let hash = 0;
        let i;
        hash = name.charCodeAt(i) + ((hash << 5) - hash);

        [...name].forEach(char => hash = char.charCodeAt(0) + ((hash << 5) - hash))
      
        let color = '#';
      
        for (i = 0; i < 3; i += 1) {
          const value = (hash >> (i * 8)) & 0xff;
          color += `00${value.toString(16)}`.substr(-2);
        }
      
        return color;
    }

    const string_avatar = () => {
        return {
          sx: {
            bgcolor: string_to_color(firstName + ' '+lastName ),
          },
          children: `${firstName[0].toLocaleUpperCase()}${lastName[0].toLocaleUpperCase()}`,
        };
      }

    return(
        <Box sx={{display:'flex' , justifyContent:'center'}} >
            <Paper elevation={5}  >
                <Card 
                id={'Card'}
                sx={{bgcolor:'#f9fcfb' , width:270 }} 
                onMouseOver={()=> change_visability(true)}
                onMouseLeave ={()=>change_visability(false)}
                >
                    <CardContent>
                        <Box sx={{display:'flex' , justifyContent:'center' , mb:2}}>
                            <Avatar {...string_avatar()}/>
                        </Box>
                        <Box sx={{display:'flex' , justifyContent:'center' , mb:1}}> 
                            <Typography variant="h4" >
                                {userName}
                            </Typography>
                        </Box> 
                        <Box sx={{display:'flex' , alignContent:'row' , justifyContent:'space-around', mb:1}}>
                            <Typography variant="h6" >
                                {firstName}
                            </Typography>
                            <Typography variant="h6" >
                                {lastName}
                            </Typography>
                        </Box>
                        <Box sx={{display:'flex' , justifyContent:'center'}}>
                            <Typography variant="h6" >
                                {email}
                            </Typography>
                        </Box>
                        {show_create_chat_button && <Box sx={{display:'flex' , justifyContent:'center' , mt:1}}>
                                <Button variant="contained" onClick={create_chat}>Create Chat</Button>
                            </Box>}
                    </CardContent>
                </Card>
            </Paper>
        </Box>
    );
}

export default UserCardDisplay