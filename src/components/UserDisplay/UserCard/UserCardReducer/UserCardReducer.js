import action_type from "./action_types"

const VisabilityAction = (value) => ({
    type:action_type.visability_changed,
    value: value
}
)

const UserCardReducer = (state , action) => {
    const new_state = {...state};
    switch(action.type){
        case action_type.visability_changed: 
            new_state.show_create_chat_button = action.value;
            break;
        default:
            break;
    }
    return new_state
}

export { VisabilityAction , UserCardReducer }