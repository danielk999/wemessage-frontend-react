import {  UserCardReducer } from './UserCardReducer';
import action_type from './action_types';

describe('user card reducer test' , ()=>{
    
    test('show buttom changed', () => {

        const state = {show_create_chat_button: false}
        expect(UserCardReducer(state,{type:action_type.visability_changed , value:true})).toStrictEqual({show_create_chat_button: true});
      
      });

})
