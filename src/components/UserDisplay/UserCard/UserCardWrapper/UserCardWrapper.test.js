import React from 'react';
import {mount} from 'enzyme';
import UserCardWrapper from './UserCardWrapper';
import {BrowserRouter} from "react-router-dom";


describe('user card wrapper test' , () =>{
    
    // test('on mouse over test', async () => {
  
    //     const component = mount(<BrowserRouter><UserCardWrapper firstName={'e'} lastName={'e'} userName={'e'} email={'e'}/></BrowserRouter>);
    //     let display = component.find('#Card').at(0)
    //     display.simulate('mouseover')
    //     component.update()
    //     display = component.find('UserCardDisplay')
    //     expect(display.prop('show_create_chat_button')).toEqual(true)
    // });

    test('on mouse leave test', async () => {
  
        const component = mount(<BrowserRouter><UserCardWrapper firstName={'e'} lastName={'e'} userName={'e'} email={'e'}/></BrowserRouter>);
        let display = component.find('#Card').at(0)
        // display.simulate('mouseleave')
        component.update()
        display = component.find('UserCardDisplay')
        expect(display.prop('show_create_chat_button')).toEqual(false)
    });

})


