import React , {useReducer} from 'react';
import UserCardDisplay from '../UserCradDisplay/UserCardDisplay';
import { useNavigate } from 'react-router';
import { UserCardReducer , VisabilityAction } from '../UserCardReducer/UserCardReducer';

const UserCardWrapper = ({firstName , lastName , userName , email}) => {
    
    const [{show_create_chat_button} , dispatch] = useReducer(UserCardReducer,{show_create_chat_button: false})
    const navigate = useNavigate();

    const change_visability = (value) =>{
        dispatch(VisabilityAction(value))
    }
    
    const create_chat = () =>{
        navigate('../chat/add',{state:{peer_user_name:userName}})
    }

    return(
        <UserCardDisplay
        firstName={firstName}
        lastName={lastName}
        userName={userName}
        email={email} 
        show_create_chat_button={show_create_chat_button}
        change_visability={change_visability}
        create_chat={create_chat}
        />
    );
}

export default UserCardWrapper