import React from 'react';
import Box from '@mui/material/Box';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import SearchIcon from '@mui/icons-material/Search';
import TextField  from '@mui/material/TextField';
import LogoutIcon from '@mui/icons-material/Logout';
import IconButton from '@mui/material/IconButton';
import { useSelector , useDispatch } from 'react-redux';
import { UsersFetchActionAsync } from '../../Redux/UserSearchState/UserSearchAction';
import { selectSearch } from '../../Redux/UserSearchState/UserSelector';
import ChatIcon from '@mui/icons-material/Chat';


const UserSearchBarDisplay = ({  logout , go_to_chat}) => {

    const dispatch = useDispatch();
    const search_change= input_event => dispatch(UsersFetchActionAsync(input_event.target.value));
    const search =  useSelector(state => selectSearch(state));

    return (
        <Box>
            <AppBar position="fixed">
                <Toolbar sx={{display:'flex' , justifyContent:'space-between'}}>
                    <Typography>WeMessage</Typography>
                    <Box sx={{width:'40vw',display: 'flex' ,alignItems:'row'}}>
                        <TextField
                            sx={{mt:2 , input:{color:'white'}}}
                            variant='standard'
                            fullWidth
                            name = 'search'
                            value={search}
                            placeholder='Search Users...'
                            onChange={search_change}
                        />
                        <SearchIcon sx={{mt:2}}></SearchIcon>
                    </Box>
                    <Box>
                        <IconButton sx={{color:'white'}} onClick={go_to_chat}>
                            <ChatIcon/>
                        </IconButton>
                        <IconButton sx={{color:'white'}} onClick={logout}>
                            <LogoutIcon/>
                        </IconButton>
                    </Box>
                </Toolbar>
            </AppBar>
            <Toolbar/>
        </Box>
    );
}


export default UserSearchBarDisplay;