import React from 'react';
import UserCardWrapper from '../UserCard/UserCardWrapper/UserCardWrapper';
import Box from '@mui/material/Box';
import Image from '../../../network2.jpg'
import { selectUsersArr } from '../../Redux/UserSearchState/UserSelector';
import { connect } from 'react-redux';


const UserListDisplay = ({users}) => {
    return(
        <Box sx={{display:'column', backgroundImage:`url(${Image})`, backgroundRepeat:'repeat'}}>
            {users.map(single_user => {
                return <Box key={single_user.userName} sx={{mb: 3}}><UserCardWrapper firstName={single_user.firstName} lastName={single_user.lastName} userName={single_user.userName} email={single_user.email} /></Box>
            })}
        </Box>
    );
}

const mapStateToProps = state =>{
    return{
        users: selectUsersArr(state)
    }
}

export default connect(mapStateToProps)(UserListDisplay);