import React from 'react';
import Box from '@mui/material/Box';
import UserListDisplay from '../../UserList/UserListDisplay';
import UserSearchBarDisplay from '../../UserSearchBar/UserSearchBarDisplay';

const UserSearchDisplay = ({logout, go_to_chat}) => {
    
    return (
        <Box >
            <UserSearchBarDisplay logout={logout}  go_to_chat={go_to_chat}/>
            <UserListDisplay/>
        </Box>
    );
}

export default UserSearchDisplay;