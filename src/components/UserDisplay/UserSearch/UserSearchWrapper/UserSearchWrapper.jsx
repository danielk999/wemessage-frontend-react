import React , { useEffect , useCallback , useMemo} from 'react';
import { useNavigate } from 'react-router';
import UserSearchDisplay from '../UserSearchDisplay/UserSearchDisplay';
import { UsersFetchActionAsync } from '../../../Redux/UserSearchState/UserSearchAction';
import { useDispatch } from 'react-redux';

const UserSearchWrapper = () => {
    const dispatch = useDispatch();
    const set_users =useCallback(() => dispatch(UsersFetchActionAsync('')) , [dispatch]);
    const navigate = useNavigate();
    const myStorage = useMemo(()=>window.localStorage,[]);

    useEffect(() => {
        set_users()
    } , [set_users]);

    const logout = () => {
        myStorage.removeItem('user_name');
        navigate('/login')
    }
    
    const go_to_chat = () => {
        navigate('/chats')
    }
    
    return (<UserSearchDisplay
        logout = {logout}
        go_to_chat={go_to_chat}
    />);
}

export default UserSearchWrapper;