import React from 'react';
import {mount} from 'enzyme';
import UserSearchWrapper from './UserSearchWrapper';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { act } from 'react-dom/test-utils';
import {BrowserRouter} from "react-router-dom";
import {Provider} from 'react-redux';
import store from '../../../Redux/Store';

describe('user search wrapper test' , ()=>{
    test('component did mount', async () => {

        const mock = new MockAdapter(axios);
        const users = [{firstName: 'daniel' , lastName:'katz' , userName:'katzu' , email:'katzdaniel11@gmail.com'}
        , {firstName: 'marco' , lastName:'polo' , userName:'marcococo' , email:'marcococo@gmail.com'}]
        mock.onGet('http://localhost:8080/users',{params:{data:''}}).reply(200 , {body:users});
        let component = null
        await act(async () =>{
            component = mount(<Provider store={store}><BrowserRouter><UserSearchWrapper/></BrowserRouter></Provider>);
        })
        component.update()
        const display = component.find('UserListDisplay')
        expect(display.prop('users')).toEqual(users)
        
    });
    
    test('input change', async () => {
    
        const mock = new MockAdapter(axios);
        const users = [{firstName: 'daniel' , lastName:'katz' , userName:'katzu' , email:'katzdaniel11@gmail.com'}
        , {firstName: 'marco' , lastName:'polo' , userName:'marcococo' , email:'marcococo@gmail.com'}]
        mock.onGet('http://localhost:8080/users',{params:{data:''}}).reply(200 , {body:[]});
        mock.onGet('http://localhost:8080/users',{params:{data:'v'}}).reply(200 , {body:users});
        let component = null
        await act(async () =>{
            component = mount(<Provider store={store}><BrowserRouter><UserSearchWrapper/></BrowserRouter></Provider>);
        })
        let display = component.find('UserSearchBarDisplay')
        const event = {target: {name: 'search', value: 'v'}};
        const test_field = display.find('input')
        await act(async () =>{
            test_field.simulate('change',event)
        })
        component.update()
        display = component.find('UserListDisplay')
        expect(display.prop('users')).toEqual(users)
        
    });

})


