import React, { useReducer, useEffect ,useMemo} from 'react';
import RegisterDisplay from '../RegDisplay/RegisterDisplay';
import { Reducer,TextFieldAction ,ErrorAction} from '../RegReducer/RegisterReducer';
import { useNavigate } from 'react-router';
import { registerApi } from '../../../Utils/Api';




const RegisterWrapper = () => {
    
    const navigate = useNavigate();
    const [state , dispatch] = useReducer(Reducer,{first_name:'' ,password:'', last_name:'' , email:'' , user_name:'' , birthday:'2021-05-24' ,error:false , error_msg:''})
    const my_storage = useMemo(()=>window.localStorage,[]);

    useEffect(() => {
        // Update the document title using the browser API
        if(my_storage.getItem('user_name') !== null){
            navigate('/home')
        }
      } , [navigate,my_storage]);
    
    const changeDateFormat = (date) =>{
        const new_date = date.split("-")
        return new_date[2]+"-"+new_date[1]+"-" +new_date[0]
    }
    
    
    const javaJson = () =>{
        return {
            userName:state.user_name,
            email:state.email,
            password:state.password,
            firstName:state.first_name,
            lastName:state.last_name,
            dob:changeDateFormat(state.birthday)
        }
    }
    
    const handleTextFieldInput = (input_event) => {
        
        dispatch(TextFieldAction(input_event.target.name , input_event.target.value))
    }

    const register = async () => {
        const response = await registerApi(javaJson())
        if(response.data.status === 'OK'){
            my_storage.setItem('user_name' , response.data.body.userName);
            navigate('/home')
        }else{
            dispatch(ErrorAction(response.data.status))
        }
        
    }
    
    return (<RegisterDisplay
        first_name = {state.first_name}
        last_name = {state.last_name}
        password = {state.password}
        email = {state.email}
        user_name = {state.user_name}
        birthday = {state.birthday}
        on_input_change = {handleTextFieldInput}
        on_button= {register}
        error= {state.error}
        error_msg = {state.error_msg}
    />);
}

export default RegisterWrapper