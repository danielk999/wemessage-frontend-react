import React from 'react';
import {mount} from 'enzyme';
import RegisterWrapper from './RegisterWrapper';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { act } from 'react-dom/test-utils';
import {BrowserRouter} from "react-router-dom";

describe('register wrapper test' , ()=>{
  const test_case = (field_name,text_field_number,value = 'e') =>{
    const component = mount(<BrowserRouter><RegisterWrapper/></BrowserRouter>);
    let display = component.find('RegisterDisplay')
    const event = {target: {name: field_name, value: value}};
    const test_field = display.find('input').at(text_field_number)
    test_field.simulate('change',event)
    component.update()
    display = component.find('RegisterDisplay')
    return display.prop(field_name)
}

  test('first name changed', () => {
    expect(test_case('first_name',0)).toEqual('e') 
  });

  test('last name changed', () => {
    expect(test_case('last_name',1)).toEqual('e')
  });

  test('user name changed', () => {
    expect(test_case('user_name',2)).toEqual('e') 
  });

  test('password changed', () => {
    expect(test_case('password',3)).toEqual('e')
  });


  test('email changed', () => {
    expect(test_case('email',4)).toEqual('e')
  });


  test('birthday changed', () => {
    expect(test_case('birthday',5,'1999-09-09')).toEqual('1999-09-09')
  });

  test('error message changed', async () => {
  
    const mock = new MockAdapter(axios);
    const data = {status:'BAD_INPUT' , body:''}
    mock.onPost('http://localhost:8080/register').reply(200,data);
    const component = mount(<BrowserRouter><RegisterWrapper/></BrowserRouter>);
    let display = component.find('RegisterDisplay')
    const button = display.find('button')
    await act(async () => {
      button.simulate('click')
    });
    component.update()
    display = component.find('RegisterDisplay')
    expect(display.prop('error')).toEqual(true)
    expect(display.prop('error_msg')).toEqual('BAD_INPUT')
  });

})


