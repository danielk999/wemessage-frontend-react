import action_type from "./action_types"

const TextFieldAction = (field,value) => ({
    type:action_type.text_field_changed,
    field:field,
    value: value
}
)

const ErrorAction = (msg) => ({
    type:action_type.error,
    error:true,
    error_msg:msg
}
)

const Reducer = (state , action) => {
    const new_state = {...state};
    switch(action.type){
        case action_type.text_field_changed: 
            new_state[action.field] = action.value;
            break;
        case action_type.error:
            new_state.error = action.error;
            new_state.error_msg = action.error_msg;
            break;
        default:
            break;
    }
    return new_state
}

export {TextFieldAction,Reducer,ErrorAction}