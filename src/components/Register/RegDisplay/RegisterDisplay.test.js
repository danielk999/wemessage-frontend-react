import React from 'react';
import {shallow} from 'enzyme';
import RegisterDisplay from './RegisterDisplay';

describe('register display test' , ()=>{
  const component = shallow(<RegisterDisplay/>);
  test('first name exists', () => {
  
    expect(component.find("#first_name").exists()).toBeTruthy();
  
  });
  test('last name exists', () => {
  
    expect(component.find("#last_name").exists()).toBeTruthy();
  
  });

  test('user name exists', () => {
  
    expect(component.find("#user_name").exists()).toBeTruthy();
  
  });

  test('email exists', () => {
  
    expect(component.find("#email").exists()).toBeTruthy();
  
  });

  test('birthday exists', () => {
  
    expect(component.find("#birthday").exists()).toBeTruthy();
  
  });

  test('password exists', () => {
  
    expect(component.find("#password").exists()).toBeTruthy();
  
  });

})
