import React from 'react';
import { TextField ,Typography,Button } from '@mui/material';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Image from '../../../img.jpg'
import { Link } from "react-router-dom";
import AlertComponent from '../../../Utils/Alert';
 
const RegisterDisplay = ({fisrt_name, last_name, user_name, password, email, birthday, on_input_change, on_button , error , error_msg}) =>{
    return(
        <Box sx={{backgroundImage:`url(${Image})`}}>
            <Container maxWidth="xs">
                <Box
                    sx={{
                    marginTop: 10,
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    bgcolor:'white',
                    boxShadow: 2,
                    borderRadius: 2,
                    }}
                >
                    <Typography sx={{ mt: 3 }} variant="h5">Register Now</Typography>
                    <Box sx={{ mt: 3 }}>
                        <Grid container spacing={2}>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    autoComplete="given-name"
                                    name="first_name"
                                    required
                                    fullWidth
                                    id="first_name"
                                    label="First Name"
                                    value = {fisrt_name}
                                    autoFocus
                                    onChange = {on_input_change}
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    required
                                    fullWidth
                                    id="last_name"
                                    label="Last Name"
                                    name="last_name"
                                    value = {last_name}
                                    autoComplete="family-name"
                                    onChange = {on_input_change}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    fullWidth
                                    id="user_name"
                                    label="User Name"
                                    name="user_name"
                                    value = {user_name}
                                    autoComplete="user"
                                    onChange = {on_input_change}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    fullWidth
                                    name="password"
                                    label="Password"
                                    type="password"
                                    id="password"
                                    value = {password}
                                    autoComplete="new-password"
                                    onChange = {on_input_change}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    fullWidth
                                    name="email"
                                    label="Email Address"
                                    id="email"
                                    value = {email}
                                    autoComplete="email@gmail.com"
                                    onChange = {on_input_change}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    name = "birthday"
                                    id="birthday"
                                    label="Birthday"
                                    type="date"
                                    value = {birthday}
                                    InputLabelProps={{
                                    shrink: true,
                                    }}
                                    onChange = {on_input_change}
                                />
                            </Grid>
                            <Grid item xs={12} sx={{justifyContent:'center' , display:'flex' , paddingBottom:'10px'}}>
                                <Button
                                    type="button"
                                    variant="contained"
                                    onClick={on_button}
                                    >
                                    Sign Up
                                </Button>
                            </Grid>
                            {error && <Grid item xs={12} sx={{justifyContent:'center' , display:'flex'}}>
                                <AlertComponent error_msg={error_msg}/>
                            </Grid>}

                            <Grid item xs={12} sx={{justifyContent:'center' , display:'flex'}}>
                                <Link to="/login">
                                    <Typography sx={{textTransform:'none' , mb: 3}} >Already registered? Login now!</Typography>
                                </Link>
                            </Grid>
                        </Grid>
                    </Box>
                </Box>
            </Container>  
        </Box>
    );
}

export default RegisterDisplay;