import React from 'react';
import {mount} from 'enzyme';
import {BrowserRouter} from "react-router-dom";
import AddChatWrapper from './AddChatWrapper';

describe('add chat wrapper test' , ()=>{
  const test_case = (field_name, value = 'e') =>{
    const component = mount(<BrowserRouter><AddChatWrapper/></BrowserRouter>);
    let display = component.find('AddChatDisplay')
    const event = {target: {name: field_name, value: value}};
    const test_field = display.find('input')
    test_field.simulate('change',event)
    component.update()
    display = component.find('AddChatDisplay')
    return display.prop(field_name)
  }


  test('chat name changed', () => {
    expect(test_case('chat_name')).toEqual('e')

  });

})
 

