import React, { useReducer , useMemo } from 'react';
import AddChatDisplay from '../AddChatDisplay/AddChatDisplay';
import { Reducer , TextFieldAction , ErrorAction } from '../AddChatReducer/AddChatReducer';
import { useNavigate } from 'react-router';
import {useLocation} from 'react-router-dom';
import { createChatApi } from '../../../Utils/Api';


const AddChatWrapper = () => {

    const location = useLocation();
    const navigate = useNavigate();
    const [state , dispatch] = useReducer(Reducer,{chat_name:'' ,error:false , error_msg:''})
    const my_storage = useMemo(()=>window.localStorage,[]);
    
    
    const javaJson = () =>{
        const current_user = my_storage.getItem('user_name')
        const peer_user_name = location.state.peer_user_name
        console.log(peer_user_name)
        const userNames = [peer_user_name , current_user]
        
        return {
            chatName:state.chat_name,
            userNames:userNames
        }
    }
    
    const handleTextFieldInput = (input_event) => {
        dispatch(TextFieldAction(input_event.target.value))
    }

    const create_chat = async () => {
        const response = await createChatApi(javaJson())
        if(response.data.status === 'OK'){
            navigate('../../chats')
        }else{
            dispatch(ErrorAction(response.data.status))
        }
    }
    
    return (<AddChatDisplay
        chat_name = {state.chat_name}
        on_input_change = {handleTextFieldInput}
        on_button= {create_chat}
        error = {state.error}
        error_msg = {state.error_msg}
    />);
}

export default AddChatWrapper