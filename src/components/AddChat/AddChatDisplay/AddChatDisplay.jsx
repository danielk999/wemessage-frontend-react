import React from 'react';
import { TextField , Typography , Button } from '@mui/material';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Image from '../../../img.jpg';
import AlertComponent from '../../../Utils/Alert';

const AddChatDisplay = ({chat_name, on_input_change, on_button , error , error_msg }) =>{
    return(
        <Box sx={{backgroundImage:`url(${Image})`}}>
            <Container maxWidth="xs">
                <Box
                    sx={{
                    marginTop: 18,
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    bgcolor:'white',
                    boxShadow: 2,
                    borderRadius: 2,
                    }}
                >
                    <Typography sx={{ mt: 6 }} variant="h5">Create New Chat</Typography>
                    <Box component="form" sx={{ mt: 3 }}>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    fullWidth
                                    id="chat_name"
                                    label="Chat Name"
                                    name="chat_name"
                                    value = {chat_name}
                                    onChange = {on_input_change}
                                />
                            </Grid>
                            <Grid item xs={12} sx={{justifyContent:'center' , display:'flex'}}>
                                <Button
                                    type="button"
                                    variant="contained"
                                    sx={{ mt: 2, mb: 3 }}
                                    onClick={on_button}
                                    >
                                    create
                                </Button>
                            </Grid>
                            {error && <Grid item xs={12} sx={{justifyContent:'center' , display:'flex' , mb:2}}>
                                <AlertComponent error_msg={error_msg} />
                            </Grid>}
                        </Grid>
                    </Box>
                </Box>
            </Container>  
        </Box>
    );
}

export default AddChatDisplay;