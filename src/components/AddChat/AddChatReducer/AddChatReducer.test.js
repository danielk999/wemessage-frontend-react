import { TextFieldAction , Reducer , ErrorAction } from './AddChatReducer';

describe('add chat reducer test' , ()=>{
  test('chat name changed', () => {

    const state = {chat_name:""}
    expect(Reducer(state,TextFieldAction("new"))).toStrictEqual({chat_name:"new"});
  
  });

  test('error changed', () => {

    const state = {error:"",error_msg:""}
    expect(Reducer(state,ErrorAction("new"))).toStrictEqual({error:true,error_msg:"new"});
  
  });

  
  test('no change', () => {
    const state = {first_name:"",last_name:""}
    const new_state = Reducer(state,{type:"no_Type", field:"" , value:""})
    expect(new_state).toStrictEqual({first_name:"",last_name:""});
  
  });

})
