import {createStore , applyMiddleware} from 'redux';
import logger from 'redux-logger';
import RootReducer from './RootReducer';
import thunk from 'redux-thunk'

const middleware = [logger ,thunk ]

const store = createStore(RootReducer , applyMiddleware(...middleware))

export default store;