import {combineReducers} from 'redux';
import UserSearchRdeucer from './UserSearchState/UserSearchReducer';
import ChatPageReducer from './ChatPageState/ChatPageReducer';

const RootReducer = combineReducers({
    users : UserSearchRdeucer,
    chats : ChatPageReducer
})

export default RootReducer;