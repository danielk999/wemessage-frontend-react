import action_type from "./action_types";

const ChatChangedAction = (id , value) => ({
    type: action_type.chat_changed,
    id: id,
    value: value
})

const MessagesInitAction = (messages) => ({
    type: action_type.message_init,
    value: messages
})

const MessageAddAction = (message) => ({
    type: action_type.message_added,
    value: message
})

const ClientChangedAction = (client) => ({
    type: action_type.client_changed,
    value: client
})

export { ChatChangedAction , MessagesInitAction , MessageAddAction , ClientChangedAction }