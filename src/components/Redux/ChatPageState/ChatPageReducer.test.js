import ChatPageReducer from './ChatPageReducer';
import action_type from './action_types';


describe('chat page reducer test',()=>{

    const initial_state ={
        chatId: -1 , 
        chatName: '',
        messages: [],
        clientRef:null
    }

    test('chat changed' , () =>{
        
        expect(ChatPageReducer(initial_state , {type: action_type.chat_changed , id:1, value: 'e'})).toEqual({
            chatId: 1 , 
            chatName: 'e',
            messages: [],
            clientRef:null
        })
    })
    
    test('initiating messages test' , () =>{

        expect(ChatPageReducer(initial_state , {type: action_type.message_init , value:[{id:1}]})).toEqual({
            chatId: -1 , 
            chatName: '',
            messages: [{id:1}],
            clientRef:null
        })
    })
    
    test('message adding test' , () =>{

        expect(ChatPageReducer(initial_state , {type: action_type.message_added,value:{id:1}})).toEqual({
            chatId: -1 , 
            chatName: '',
            messages: [{id:1}],
            clientRef:null
        })
    })
    
    test('client changed test' , () =>{

        expect(ChatPageReducer(initial_state , {type: action_type.client_changed , value:{}})).toEqual({
            chatId: -1 , 
            chatName: '',
            messages: [],
            clientRef:{}
        })
    })

})