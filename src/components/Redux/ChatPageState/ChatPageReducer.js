import action_type from "./action_types";



const initial_state ={
    chatId: -1 , 
    chatName: '',
    messages: [],
    clientRef:null
}

const ChatPageReducer = (state = initial_state , action) => {
    const new_state = {...state};
    switch(action.type){
        
        case action_type.chat_changed: 
            new_state.chatId = action.id;
            new_state.chatName = action.value;
            new_state.messages = [];
            break;
        
        case action_type.message_added:
            new_state.messages = [...state.messages , action.value]
            console.log(new_state)
            break;
        
        case action_type.message_init:
            console.log(action.value)
            new_state.messages = action.value;
            break;
        
        case action_type.client_changed:
            new_state.clientRef = action.value;
            break;

        default: 
            break;
    }
    return new_state
}

export default ChatPageReducer
