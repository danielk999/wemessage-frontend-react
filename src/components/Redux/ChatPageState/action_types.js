const action_type = {
    chat_changed: 'CHAT_CHANGED' , 
    message_init: 'MESSAGES_INITIALIZED',
    message_added: 'MESSAGE_ADDED',
    client_changed:'CLIENT_CHANGED'
}

export default action_type