import { createSelector } from '@reduxjs/toolkit'; 

const selectChats = state => state.chats

const selectId = createSelector(
    [selectChats],
    (chats) => chats.chatId
);

const selectName = createSelector(
    [selectChats],
    (chats) => chats.chatName
);

const selectMessages = createSelector(
    [selectChats],
    (chats) => chats.messages
);

const selectClient = createSelector(
    [selectChats],
    (chats) => chats.clientRef
);

export {selectId , selectName , selectMessages , selectClient}