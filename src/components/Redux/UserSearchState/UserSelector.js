import { createSelector } from '@reduxjs/toolkit'; 

const selectUsers = state => state.users

const selectSearch = createSelector(
    [selectUsers],
    (users) => users.search
);

const selectUsersArr = createSelector(
    [selectUsers],
    (users) => users.users
);

export { selectSearch , selectUsersArr };