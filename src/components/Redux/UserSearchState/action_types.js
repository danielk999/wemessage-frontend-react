const action_type = {
    text_field_changed:'INPUT_CHANGED',
    users_changed_success:'USERS_CHANGED_SUCCESS',
    users_changed_fail:'USERS_CHANGED_FAIL',
    users_changed_start:'USERS_CHANGED_START'
}

export default action_type