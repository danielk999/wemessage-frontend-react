import UserSearchReducer from './UserSearchReducer';
import action_type from './action_types';

describe('user search reducer test' , ()=>{
    test('search bar changed' , () =>{
        const state = {search:'', users:[]}
        expect(UserSearchReducer(state , {type: action_type.text_field_changed , value: 'e'})).toEqual({search:'e', users:[]})
    })
    
    test('user fetch start test' , () =>{
        const state = {isFetching: false}
        expect(UserSearchReducer(state , {type: action_type.users_changed_start})).toEqual({isFetching: true})
    })
    
    test('user fetch success test' , () =>{
        const new_users = [{new_user:'daniel'}]
        const state = {isFetching: true , users: []}
        expect(UserSearchReducer(state , {type: action_type.users_changed_success,value:new_users})).toEqual({isFetching: false , users:[{new_user:'daniel'}]})
    })
    
    test('user fetch fail test' , () =>{
        const state = {isFetching: true , error_message: ''}
        expect(UserSearchReducer(state , {type: action_type.users_changed_fail , value:'e'})).toEqual({isFetching: false , error_message: 'e'})
    })

})

