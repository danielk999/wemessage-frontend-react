import action_type from "./action_types";



const initial_state ={
    search: '',
    users: [],
    isFetching: false,
    error_message: ''
}

const UserSearchReducer = (state = initial_state , action) => {
    const new_state = {...state};
    switch(action.type){
        
        case action_type.text_field_changed: 
            new_state.search = action.value;
            break;
        
        case action_type.users_changed_success:
            new_state.users = action.value;
            new_state.isFetching = false;
            break;
        
        case action_type.users_changed_start:
            new_state.isFetching = true;
            break;
        
        case action_type.users_changed_fail:
            new_state.isFetching = false;
            new_state.error_message = action.value
            break;
        
        default: 
            break;
    }
    return new_state
}

export default UserSearchReducer
