import action_type from "./action_types";
import { getUserApi } from '../../../Utils/Api'

const TextFieldAction = value => ({
    type: action_type.text_field_changed,
    value: value
}
)

const UsersActionSuccess = arr => ({
    type: action_type.users_changed_success,
    value: arr
}
)

const UsersActionStart = () => ({
    type: action_type.users_changed_start
}
)

const UsersActionFail = err => ({
    type: action_type.users_changed_fail,
    value: err
}
)

const UsersFetchActionAsync = input => {
    return async dispatch => {

        dispatch(TextFieldAction(input))
        dispatch(UsersActionStart())
        
        try{

            const users_data = await getUserApi({params:{data:input}})
            const user_name = window.localStorage.getItem('user_name')
            dispatch(UsersActionSuccess(users_data.data.body.filter(user => user.userName !== user_name)))

        }catch(err){ 

            dispatch(UsersActionFail(err.message))

        }
        

    }

}


export {TextFieldAction , UsersActionSuccess , UsersActionStart , UsersActionFail , UsersFetchActionAsync}