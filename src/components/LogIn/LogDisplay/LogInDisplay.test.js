import React from 'react';
import {shallow} from 'enzyme';
import LogInDisplay from './LogInDisplay';

describe('log in display test' , ()=>{
  const component = shallow(<LogInDisplay/>);

  test('user name exists', () => {
  
    expect(component.find("#user_name").exists()).toBeTruthy();
  
  });

  test('password exists', () => {
  
    expect(component.find("#password").exists()).toBeTruthy();
  
  });

})

