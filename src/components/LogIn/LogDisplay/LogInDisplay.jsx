import React from 'react';
import { TextField ,Typography,Button } from '@mui/material';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Image from '../../../img.jpg'
import { Link } from "react-router-dom";
import AlertComponent from '../../../Utils/Alert';

const LogInDisplay = ({user_name, password, on_input_change, on_button , error , error_msg}) =>{
    return(
        <Box sx={{backgroundImage:`url(${Image})`}}>
            <Container maxWidth="xs">
                <Box
                    sx={{
                    marginTop: 18,
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    bgcolor:'white',
                    boxShadow: 2,
                    borderRadius: 2,
                    }}
                >
                    <Typography sx={{ mt: 6 }} variant="h5">Log In Now</Typography>
                    <Box sx={{ mt: 3 }}>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    fullWidth
                                    id="user_name"
                                    label="User Name"
                                    name="user_name"
                                    autoComplete="user"
                                    value = {user_name}
                                    onChange = {on_input_change}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    fullWidth
                                    name="password"
                                    label="Password"
                                    type="password"
                                    id="password"
                                    autoComplete="new-password"
                                    value = {password}
                                    onChange = {on_input_change}
                                />
                            </Grid>
                            <Grid item xs={12} sx={{justifyContent:'center' , display:'flex'}}>
                                <Button
                                    type="button"
                                    variant="contained"
                                    sx={{ mt: 2, mb: 3 }}
                                    onClick={on_button}
                                    >
                                    Log In
                                </Button>
                            </Grid>

                            {error && <Grid item xs={12} sx={{justifyContent:'center' , display:'flex'}}>
                                <AlertComponent error_msg={error_msg} />
                            </Grid>}

                            <Grid item xs={12} sx={{justifyContent:'center' , display:'flex'}}>
                            <Link to="/">
                                <Typography sx={{textTransform:'none' , mb: 5}} >Not registered yet? Register now!</Typography>
                            </Link>
                        </Grid>
                        </Grid>
                    </Box>
                </Box>
            </Container>  
        </Box>
    );
}

export default LogInDisplay;