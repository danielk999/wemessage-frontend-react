import React from 'react';
import {mount} from 'enzyme';
import LogInWrapper from './LogInWrapper';
import {BrowserRouter} from "react-router-dom";
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { act } from 'react-dom/test-utils';

describe('log in wrapper test' , () =>{
  const test_case = (field_name, text_field_number, value = 'e') =>{
    const component = mount(<BrowserRouter><LogInWrapper/></BrowserRouter>);
    let display = component.find('LogInDisplay')
    const event = {target: {name: field_name, value: value}};
    const test_field = display.find('input').at(text_field_number)
    test_field.simulate('change',event)
    component.update()
    display = component.find('LogInDisplay')
    return display.prop(field_name)
  }


  test('user name changed', () => {
    expect(test_case('user_name',0)).toEqual('e')

  });

  test('password changed', () => {
    expect(test_case('password',1)).toEqual('e')

  });

  test('error message changed', async () => {
    
    const mock = new MockAdapter(axios);
    const data = {status:'BAD_INPUT' , body:''}
    mock.onPost('http://localhost:8080/login').reply(200,data);
    const component = mount(<BrowserRouter><LogInWrapper/></BrowserRouter>);
    let display = component.find('LogInDisplay')
    const button = display.find('button')
    await act( async () => {
      button.simulate('click')
    });
    component.update()
    display = component.find('LogInDisplay')
    expect(display.prop('error')).toEqual(true)
    expect(display.prop('error_msg')).toEqual('BAD_INPUT')
  });

})


