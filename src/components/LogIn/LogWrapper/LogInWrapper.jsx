import React, { useReducer , useEffect , useMemo} from 'react';
import LogInDisplay from '../LogDisplay/LogInDisplay';
import { Reducer,TextFieldAction , ErrorAction } from '../LogReducer/LogInReducer';
import { useNavigate } from 'react-router';
import { loginApi } from '../../../Utils/Api'



const LogInWrapper = () => {

    const navigate = useNavigate();
    const [state , dispatch] = useReducer(Reducer,{password:'' , user_name:'' ,error:false , error_msg:''})
    const my_storage = useMemo(()=>window.localStorage,[]);
    
    useEffect(() => {
        if(my_storage.getItem('user_name') !== null){
            navigate('/home')
        }
      },[navigate,my_storage]);
    
    const javaJson = () =>{
        return {
            userName:state.user_name,
            password:state.password,
        }
    }
    
    const handleTextFieldInput = (input_event) => {
        dispatch(TextFieldAction(input_event.target.name , input_event.target.value))
    }

    const log_in = async () => {
        const response = await loginApi(javaJson())
        if(response.data.status === 'OK'){
            my_storage.setItem('user_name' , response.data.body.userName);
            navigate('/home')
        }else{
            dispatch(ErrorAction(response.data.status))
        }
    }
    
    return (<LogInDisplay
        user_name = {state.user_name}
        password = {state.password}
        on_input_change = {handleTextFieldInput}
        on_button= {log_in}
        error = {state.error}
        error_msg = {state.error_msg}
    />);
}

export default LogInWrapper