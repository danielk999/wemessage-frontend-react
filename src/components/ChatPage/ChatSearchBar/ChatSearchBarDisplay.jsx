import React from 'react';
import Box from '@mui/material/Box';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import LogoutIcon from '@mui/icons-material/Logout';
import IconButton from '@mui/material/IconButton';
import SearchIcon from '@mui/icons-material/Search';


const ChatSearchBarDisplay = ({logout , search_page}) => {
    return (
        <Box>
            <AppBar position="fixed">
                <Toolbar sx={{display:'flex' , justifyContent:'space-between'}}>
                    <Typography>WeMessage</Typography>
                    <Box>
                        <IconButton sx={{color:'white'}} onClick={search_page}>
                            <SearchIcon />
                        </IconButton>
                        <IconButton sx={{color:'white'}} onClick={logout}>
                            <LogoutIcon />
                        </IconButton>
                    </Box>
                </Toolbar>
            </AppBar>
            <Toolbar/>
        </Box>
    );
}


export default ChatSearchBarDisplay;