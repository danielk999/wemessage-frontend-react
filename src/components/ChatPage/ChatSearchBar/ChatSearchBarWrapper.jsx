import React, {useMemo} from 'react';
import ChatSearchBarDisplay from './ChatSearchBarDisplay';
import { useNavigate } from 'react-router';

const ChatSearchBarWrapper = () => {

    const navigate = useNavigate();
    const myStorage = useMemo(()=>window.localStorage,[]);

    const log_Out = () =>{
        myStorage.removeItem('user_name');
        navigate('/login')
    }

    const search = () =>{
        navigate('/home')
    }

    return (
        <ChatSearchBarDisplay
        logout={log_Out}
        search_page={search}
        />
    );
}


export default ChatSearchBarWrapper;