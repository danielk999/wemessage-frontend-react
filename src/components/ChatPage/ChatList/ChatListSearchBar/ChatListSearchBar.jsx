import React from 'react';
import Box from '@mui/material/Box';
import SearchIcon from '@mui/icons-material/Search';
import TextField  from '@mui/material/TextField';

const ChatListSearchBar = ({search , on_change }) => {
    return (
        <Box sx={{width:'80%', display: 'flex' , justifyContent:'center' , ml:3}}>
            <TextField
                sx={{mt:2 , justifyContent:'center'}}
                variant='filled'
                fullWidth
                name = 'search'
                value={search}
                placeholder='Search Chats...'
                onChange={on_change}
            />
            <SearchIcon sx={{mt:5}}></SearchIcon>
        </Box>
    );
}

export default ChatListSearchBar;