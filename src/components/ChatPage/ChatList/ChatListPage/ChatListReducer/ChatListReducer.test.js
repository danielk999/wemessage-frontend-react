import { ChatListReducer } from './ChatListReducer'
import action_type from './action_types';

describe('chat list reducer test' , () =>{

    test('search bar changed' , () =>{
        const state = {search:'', filtered_chats:[]}
        expect(ChatListReducer(state , {type: action_type.text_field_changed , value: 'e'})).toEqual({search:'e', filtered_chats:[]})
    })
    
    test('filtered chats changed' , () =>{
        const state = {search:'e', filtered_chats:[]}
        expect(ChatListReducer(state , {type: action_type.filtered_chats , value: [{chatId:0 , chatName:'chat1'}]})).toEqual({search:'e', filtered_chats:[{chatId:0 , chatName:'chat1'}]})
    })

    test(' chats changed' , () =>{
        const state = {chats:[] , search:'e', filtered_chats:[]}
        expect(ChatListReducer(state , {type: action_type.global_chats_init , value: [{chatId:0 , chatName:'chat1'}]})).toEqual({chats:[{chatId:0 , chatName:'chat1'}], search:'e', filtered_chats:[]})
    })
})


