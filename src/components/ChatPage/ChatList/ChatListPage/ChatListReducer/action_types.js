const action_type = {
    text_field_changed:'INPUT_CHANGED',
    filtered_chats:'FILTERED_CHATS',
    global_chats_init:'CHATS_INIT'
}

export default action_type