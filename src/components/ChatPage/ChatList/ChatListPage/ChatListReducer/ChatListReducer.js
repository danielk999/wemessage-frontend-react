import action_type from "./action_types"

const TextFieldAction = (value) => ({
    type:action_type.text_field_changed,
    value: value
}
)

const FilterAction = (value) => ({
    type:action_type.filtered_chats,
    value: value
}
)

const InitChatsAction = (value) => ({
    type:action_type.global_chats_init,
    value: value
}
)


const ChatListReducer = (state , action) => {
    const new_state = {...state};
    switch(action.type){
        case action_type.text_field_changed: 
            new_state.search = action.value;
            break;
        case action_type.filtered_chats:
            new_state.filtered_chats = action.value;
            break;
        case action_type.global_chats_init:
            new_state.chats = action.value;
            break;
        default:
            break;
    }
    return new_state
}

export {TextFieldAction , ChatListReducer , FilterAction , InitChatsAction}