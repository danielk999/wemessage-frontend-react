import React , { useEffect , useReducer , useCallback , useRef  } from 'react';
import ChatListDisplay from '../ChatListDisplay/ChatListDisplay'
import Box from '@mui/material/Box';
import {TextFieldAction , ChatListReducer , FilterAction , InitChatsAction} from '../ChatListReducer/ChatListReducer';
import {  useSelector ,useDispatch } from 'react-redux';
import { ClientChangedAction , MessageAddAction } from '../../../../Redux/ChatPageState/Actions';
import {selectId } from '../../../../Redux/ChatPageState/ChatSelector';
import { getChatList } from '../../../../../Utils/Api';
import init_socket from '../../../../../Utils/socket';


const ChatListWrapper = () =>{

    const [{chats , search , filtered_chats} , dispatch] = useReducer(ChatListReducer,{chats:[] ,search:'' , filtered_chats:[]})
    const chatId = useRef(null)
    chatId.current = useSelector(selectId);
    const global_dispatch = useDispatch(); 
    const client_changed= useCallback(client => global_dispatch(ClientChangedAction(client)),[global_dispatch]);
    const add_message= useCallback(message => global_dispatch(MessageAddAction(message)),[global_dispatch]);
    let socket = useRef(null);

    const handle_new_msg = useCallback( (message ) =>{
        try{
            const message_content = JSON.parse(message.body).body
            if(chatId.current === message_content.chatId){
                add_message(message_content)
            }
        }catch(e){
            console.log(e)
        }
    },[add_message , chatId]);

    const set_client =useCallback(client =>{
        client_changed(client)
    },[client_changed]);

    const connect_socket = useCallback (() =>{
        if(!socket.current){
            socket.current = init_socket(chats , handle_new_msg);
            set_client(socket)
        }
    },[socket , set_client , chats , handle_new_msg]);

    useEffect(()=>{
        if(chats.length !== 0){
            connect_socket()
        }
        
    },[connect_socket , chats]);


    useEffect(()=>{
        const async_use_effect = async () => {
            const userName = window.localStorage.getItem('user_name');
            const chats_response = await getChatList({params:{userName:userName}})
            dispatch(InitChatsAction(chats_response.data.body));
            dispatch(FilterAction(chats_response.data.body))
        };
        async_use_effect();
    } , [dispatch])

    const filter = input_event => {
        const filtered_chats = chats.filter(single_chat =>{
            return single_chat.chatName.indexOf(input_event.target.value) >= 0;
        })
        dispatch(FilterAction(filtered_chats));
        dispatch(TextFieldAction(input_event.target.value))
    }
   
    return(
        <Box>
            <ChatListDisplay
            chats={filtered_chats}
            search={search}
            on_change = {filter}
            />
        </Box>
    );
}


export default ChatListWrapper;
