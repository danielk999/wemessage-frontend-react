import React from 'react';
import {mount} from 'enzyme';
import ChatListWrapper from'./ChatListWrapper';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { act } from 'react-dom/test-utils';
import {BrowserRouter} from "react-router-dom";
import {Provider} from 'react-redux';
import store from '../../../../Redux/Store';


const fakeLocalStorage = (function () {
    let store = {};
  
    return {
      getItem: function (key) {
        return '';
      },
      setItem: function (key, value) {
        store[key] = value.toString();
      },
      removeItem: function (key) {
        delete store[key];
      },
      clear: function () {
        store = {};
      }
    };
  })();

describe('test chat list wrapper' , () =>{
 

    beforeAll(()=>{
        Object.defineProperty(window, 'localStorage', {
            value: fakeLocalStorage,
        });
        const mock = new MockAdapter(axios);
        const data = {status:'OK' , body:[{chatName:'e' , chatId:1},{chatName:'v' , chatId:2}]}
        mock.onGet('http://localhost:8080/chats/get', {params:{userName:''}}).reply(200,data);
    })

    test('search changed', async () => {
        
        let component = null;

        await act(async ()=>{
            component = mount(<Provider store={store}><BrowserRouter><ChatListWrapper/></BrowserRouter></Provider>);

        })
        
        let display = component.find('ChatListDisplay')
        const event = {target: {name: 'search', value: 'e'}};
        const test_field = display.find('input')
        await act( async ()=>{
            test_field.simulate('change',event)

        })

        component.update()
        display = component.find('ChatListDisplay')
        expect(display.prop('search')).toEqual('e')
      
      });

      test('component did mount', async () => {
        
        let component = null;

        await act(async ()=>{
            component = mount(<Provider store={store}><BrowserRouter><ChatListWrapper/></BrowserRouter></Provider>);

        })
        component.update()
        const display = component.find('ChatListDisplay')
        expect(display.prop('chats')).toEqual([{chatName:'e' , chatId:1},{chatName:'v' , chatId:2}])
      
      });

      test('filter mount', async () => {
        
        let component = null;

        await act(async ()=>{
            component = mount(<Provider store={store}><BrowserRouter><ChatListWrapper/></BrowserRouter></Provider>);

        });

        let display = component.find('ChatListDisplay')
        const event = {target: {name: 'search', value: 'e'}};
        const test_field = display.find('input')
        await act( async ()=>{
            test_field.simulate('change',event)

        });

        component.update()
        display = component.find('ChatListDisplay')
        expect(display.prop('chats')).toEqual([{chatName:'e' , chatId:1}])
      
      });

})



