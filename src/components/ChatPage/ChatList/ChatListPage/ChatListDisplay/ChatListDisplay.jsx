import React from 'react';
import Box from '@mui/material/Box';
import ChatCardWrapper from '../../ChatCard/ChatCardWrapper/ChatCardWrapper'
import ChatListSearchBar from '../../ChatListSearchBar/ChatListSearchBar';
import Paper from '@mui/material/Paper';

const ChatListDisplay = ({chats , search , on_change}) =>{
    return(
        <Box sx={{mb:1}}>
            <ChatListSearchBar search={search} on_change={on_change} />
            <Paper sx={{overflow:'auto', height:'80.4vh'}}>
                {chats.map(single_chat =>{
                    return <Box key={single_chat.id}><ChatCardWrapper chatId={single_chat.id} chatName={single_chat.chatName} /></Box>
                })}
            </Paper>
        </Box>
    );
}

export default ChatListDisplay;
