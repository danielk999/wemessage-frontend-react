import React from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import Avatar from '@mui/material/Avatar';
import GroupsIcon from '@mui/icons-material/Groups';
import Button from '@mui/material/Button';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';

const ChatCardDisplay = ({chatName , on_click}) => {
    return(
        <Box  sx={{width:'100%'}}>
            <Button onClick={on_click}  sx={{display:'flex', width:'100%'  ,'&:hover':{background: "#69d4fd"}}}>
                <Card  sx={{bgcolor:'#f9fcfb' , display:'flex' , width:'100%'}} >
                    <List>
                        <CardContent sx={{display:'flex' , justifyContent:'center' , alignItems:'row'}} >
                            <ListItem>
                                <Box sx={{display:'flex' , justifyContent:'center'}}>
                                    <Avatar>
                                        <GroupsIcon/>
                                    </Avatar>
                                </Box>
                            </ListItem>
                            <ListItem >
                                <Box sx={{display:'flex' , justifyContent:'center' , mb:1 , mt:2}}> 
                                    <Typography variant="h6" sx={{display:'flex' , alignItems:'row'}}>
                                        {chatName}
                                    </Typography>
                                </Box> 
                            </ListItem>
                        </CardContent>
                    </List>
                </Card>
            </Button>
        </Box>
    );
}

export default ChatCardDisplay