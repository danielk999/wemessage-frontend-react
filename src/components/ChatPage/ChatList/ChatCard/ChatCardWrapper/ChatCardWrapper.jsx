import React from 'react';
import ChatCardDisplay from '../ChatCardDisplay/ChatCardDisplay';
import {ChatChangedAction} from '../../../../Redux/ChatPageState/Actions';
import { useDispatch } from 'react-redux';

const ChatCardWrapper = ({chatName , chatId }) => {
    const dispatch = useDispatch();
    const select_chat= (id , name) => dispatch(ChatChangedAction(id,name))
    
    const chat_selected = () => {
        select_chat(chatId , chatName)
    }

    return(
        <ChatCardDisplay
        chatName={chatName}
        on_click={chat_selected}
        />
    );
}


export default ChatCardWrapper;