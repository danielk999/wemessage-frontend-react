import React from 'react';
import {mount} from 'enzyme';
import ChatCardWrapper from'./ChatCardWrapper';
import { act } from 'react-dom/test-utils';
import {BrowserRouter} from "react-router-dom";
import {Provider} from 'react-redux';
import configureMockStore from 'redux-mock-store';


describe('test chat card wrapper' , () =>{
    const middlewares = [];
    const mockStore = configureMockStore(middlewares);

    let state = {
    chats: {
        chatId: 1 , 
        chatName: '',
        messages: [],
        clientRef:null
    }
    };

    const store = mockStore(() => state);

      test('redux store changed', async () => {
        
        let component = null;

        await act(async ()=>{
            component = mount(<Provider store={store}><BrowserRouter><ChatCardWrapper chatId={1} chatName={"hello"}/></BrowserRouter></Provider>);

        });

        const display = component.find('ChatCardDisplay')
        const test_field = display.find('button')
        await act( async ()=>{
            test_field.simulate('click')

        });

        const actions = store.getActions();
        const expectedAction = {type: 'CHAT_CHANGED', id: 1,value: "hello"}
        expect(actions).toEqual([expectedAction])
        
      
      });

})
