import React from 'react';
import {mount} from 'enzyme';
import SingleChatWrapper from'./SingleChatWrapper';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { act } from 'react-dom/test-utils';
import {BrowserRouter} from "react-router-dom";
import {Provider} from 'react-redux';
import configureMockStore from 'redux-mock-store';




const fakeLocalStorage = (function () {
    let local_store = {};
  
    return {
      getItem: function (key) {
        return '';
      },
      setItem: function (key, value) {
        local_store[key] = value.toString();
      },
      removeItem: function (key) {
        delete local_store[key];
      },
      clear: function () {
        local_store = {};
      }
    };
  })();

const middlewares = [];
const mockStore = configureMockStore(middlewares);

let state = {
  chats: {
    chatId: 1 , 
    chatName: '',
    messages: [],
    clientRef:null
  }
};

const store = mockStore(() => state);

describe('test chat wrapper' , () =>{
 
    beforeAll(()=>{
        Object.defineProperty(window, 'localStorage', {
            value: fakeLocalStorage,
        });
        const mock = new MockAdapter(axios);
        const data = {status:'OK' , body:[{body:'e' , id:1 , date:'15-5-2021'}]}
        mock.onGet('http://localhost:8080/messages/get' , {params:{chatId:-1}}).reply(200,data);
        mock.onGet('http://localhost:8080/messages/get' , {params:{chatId:1}}).reply(200,data);
    })

    test('messages changed', async () => {
        
        let component = null;

        await act(async ()=>{
            component = mount(<Provider store={store}><BrowserRouter><SingleChatWrapper/></BrowserRouter></Provider>);

        })

        const actions = store.getActions();
        const expectedAction = {type: 'MESSAGES_INITIALIZED',value: [{body:'e' , id:1 , date:'15-5-2021'}]}
        expect(actions).toEqual([expectedAction])
      
      });

      test('text field changed', async () => {
        
        let component = null;

        await act(async ()=>{
            component = mount(<Provider store={store}><BrowserRouter><SingleChatWrapper/></BrowserRouter></Provider>);

        })
        
        let display = component.find('SingleChatDisplay')
        const event = {target: {value: 'e'}};
        const test_field = display.find('input')
        await act( async ()=>{
            test_field.simulate('change',event)

        })

        component.update()
        display = component.find('SingleChatDisplay')
        expect(display.prop('message_input')).toEqual('e')
      
      });


})
