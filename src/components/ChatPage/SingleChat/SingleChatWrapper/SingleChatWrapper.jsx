import React, { useEffect , useReducer , useCallback } from 'react';
import SingleChatDisplay from '../SingleChatDisplay/SingleChatDisplay';
import { selectId , selectClient } from '../../../Redux/ChatPageState/ChatSelector';
import { useSelector , useDispatch } from 'react-redux';
import { MessagesInitAction } from '../../../Redux/ChatPageState/Actions';
import { TextFieldAction , MessageReducer } from '../SingleChatReducer/SingleChatReducer';
import { getdMesagesApi } from '../../../../Utils/Api';
import current_date from '../../../../Utils/DateFormater';

const SingleChatWrapper = () => {

    const chatId= useSelector(state => selectId(state));
    const clientRef= useSelector(state => selectClient(state));
    const global_dispatch = useDispatch(); 
    const init_messages=useCallback(messages => global_dispatch(MessagesInitAction(messages)) , [global_dispatch]);
    
    const [state , dispatch] = useReducer(MessageReducer,{message_input:''})
    const my_storage = window.localStorage;
    const userName = my_storage.getItem('user_name')

    useEffect(() =>{
        const async_messages_init = async() =>{
            const response = await getdMesagesApi({params:{chatId:chatId}})
            if(response.data.status === 'OK'){
                init_messages(response.data.body)
            }
        };
        async_messages_init();
    },[chatId , init_messages])

    const on_change = input_event =>{
        dispatch(TextFieldAction(input_event.target.value))
    }

    const create_message = body =>{
        return JSON.stringify({
            date:current_date(),
            body:body,
            senderUserName:userName,
            chatId:chatId.toString()
        })
    }

    const send_message = () =>{
        clientRef.current.send(`/app/room/${chatId}` ,{}, create_message(state.message_input))
        dispatch(TextFieldAction(''))
    }


    return(
        <SingleChatDisplay
        message_input={state.message_input}
        onInputChange={on_change}
        senderUser = {userName}
        send={send_message}
        />
    );
}


export default SingleChatWrapper;