import React from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Image from '../../../../chat_image.jpg';
import TextField  from '@mui/material/TextField';
import SendIcon from '@mui/icons-material/Send';
import Button from '@mui/material/Button';
import {selectId , selectName , selectMessages} from '../../../Redux/ChatPageState/ChatSelector';
import { useSelector } from 'react-redux';
import MyMessage from './MyMessage';
import OtherMessage from './OtherMessage';

const SingleChatDisplay = ({message_input , onInputChange , senderUser , send}) => {
    
    const chatId= useSelector(state => selectId(state))
    const chatName= useSelector(state => selectName(state))
    const messages= useSelector(state => selectMessages(state))

    const message_html = (single_message , userName) =>{
        if(single_message.senderUserName === userName){
            return (
                <Box key={single_message.id}>
                    <MyMessage senderUserName={single_message.senderUserName} body={single_message.body} date={single_message.date}/>
                </Box>)
        }else{
            return(
                <Box key={single_message.id}>
                    <OtherMessage senderUserName={single_message.senderUserName} body={single_message.body} date={single_message.date}/>
                </Box>)
        }
    }

    return(
        
        <Box sx={{width:'100%' , justifyContent:'space-between'  , flexDirection:'column' , alignItems:'center'}}>
        {chatId !== -1 &&
            <Box>
                <Typography  sx={{display:'flex' , justifyContent:'center'  , fontFamily:'Cursive'}} variant='h1'> {chatName} </Typography>
                <Box sx={{display:'flex' , overflowY:'scroll', height:'60vh' , backgroundImage:`url(${Image})` , flexDirection:'column-reverse'}}>
                    <Box>
                        {messages.map(single_message =>{
                            return (
                                message_html(single_message , senderUser )
                            )
                        })}
                    </Box>
                </Box>
                <Box sx={{display:'flex' , justifyContent:'center' , mt:3}}>
                    <TextField 
                        sx={{display:'flex' , width:'85%' , mr:1}}
                        name="send message"
                        id="send_message"
                        label="Send Message"
                        autoFocus
                        value={message_input}
                        onChange={onInputChange}
                    />
                    <Button onClick={send} variant="contained" endIcon={<SendIcon />}>
                        Send
                    </Button>
                </Box>
            </Box>
        }
        </Box>
    );
}

export default SingleChatDisplay;