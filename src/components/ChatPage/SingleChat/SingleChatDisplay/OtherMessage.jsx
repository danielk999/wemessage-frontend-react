import React from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';

const OtherMessage = ({senderUserName , body , date}) => {
    
    return(
        <Box sx={{minWidth:'400px' ,display:'flex' , justifyContent:'start' , ml:3 , mb:2}}>
            <Card sx={{ borderRadius:'30px' , bgcolor:'#8f8fff'}}>
                <CardContent>
                    <Typography sx={{justifyContent:'center' , display:'flex' , fontFamily:'Monospace'}} variant='h5'> {senderUserName} </Typography>
                    <Typography paragraph={true} variant='s'> {body} </Typography>
                    <Typography sx={{justifyContent:'end' , display:'flex'}} variant='subtitle2'> {date} </Typography>
                </CardContent>
            </Card>
        </Box>
    );
}

export default OtherMessage;