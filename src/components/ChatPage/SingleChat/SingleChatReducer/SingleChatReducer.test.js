import {MessageReducer} from './SingleChatReducer';
import action_type from './action_types';


describe('message reducer test',()=>{

    const initial_state ={
        message_input:''
    }

    test('message input changed' , () =>{
        
        expect(MessageReducer(initial_state , {type: action_type.text_field_changed , value: 'e'})).toEqual({
            message_input: 'e' , 
        })
    })


})