import action_type from './action_types';

const TextFieldAction = (value) => ({
    type:action_type.text_field_changed,
    value: value
}
)

const MessageReducer = (state , action) => {
    const new_state = {...state};
    switch(action.type){
        case action_type.text_field_changed: 
            new_state.message_input = action.value;
            break;
        default:
            break;
    }
    return new_state
}

export { TextFieldAction , MessageReducer }