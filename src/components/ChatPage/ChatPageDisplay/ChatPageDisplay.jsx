import React from 'react';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import ChatListWrapper from '../ChatList/ChatListPage/ChatListWrapper/ChatListWrapper';
import ChatSearchBarWrapper from '../ChatSearchBar/ChatSearchBarWrapper';
import SingleChatWrapper from '../SingleChat/SingleChatWrapper/SingleChatWrapper';

const ChatPageDisplay = () =>{
    return(
        <Box >
            <Paper elevation={4} sx={{ml:2 , mr:2 , mb:1}}>
                <ChatSearchBarWrapper/>
                <Grid container>
                    <Grid item xs={5} md={3} >
                        <Paper sx={{height:'90vh' , border:1 , borderRadius:0}}
                        variant='outlined'>
                            <Box  >
                                <ChatListWrapper />
                            </Box>
                        </Paper>
                    </Grid>
                    <Grid item xs={7} md={9} >
                        <Paper sx={{height:'90vh' , border:1 , borderRadius:0}}
                        variant='outlined'>
                            <SingleChatWrapper/>
                        </Paper>
                    </Grid>
                </Grid>
            </Paper>
        </Box>
    );
}

export default ChatPageDisplay;