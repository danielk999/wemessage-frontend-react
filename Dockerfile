FROM node:13.12.0-alpine

# set working directory
WORKDIR /usr/src/app

# install app dependencies
COPY package.json ./
COPY package-lock.json ./

# add app
COPY . ./

# start app
CMD ["npm", "start"]